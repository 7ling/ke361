<?php
namespace Admin\Controller;

use Admin\Controller\AdminController;
use Admin\Model\GoodsModel;
use TopSDK\Api\TopApi;

class GoodsController extends AdminController
{
    public function index(){
       $cateId = I('cate_id');
       if($cateId>0){
           $where = array('cate_id'=>$cateId);
       }
        $goodsList = $this->lists('Goods',$where,'sort desc , id desc',array());
        $this->assign('goodsList', $goodsList);
        $this->assign('category',D('CategoryGoods')->getGoodsCategory(1));
        $this->display();
    }
    public function edit(){
      
		$goodsId = I('id');
        if(IS_POST){
            $goods['title'] = I('title');          
            $goods['cate_id'] = I('cate_id');
            $goods['tid'] = I('tid');
            $goods['nick'] = I('nick');
            $goods['seo_title'] = I('seo_title');
            $goods['seo_keywords'] = I('seo_keywords');
            $goods['seo_description'] = I('seo_description');
            $goods['goods_type'] = I('goods_type');
            $goods['price'] = I('post.price', 0.00, 'floatval');        
            $goods['discount_price'] = I('post.discount_price', 0.00, 'floatval');
            $goods['click_url'] = I('click_url');
           
            $goods['status'] = I('status');
            $goods['item_body'] = I('item_body');
            $goods['pic_url'] = I('pic_url');
 
            if (!$goods['title'])
                $this->error('请填写商品名称');
            if (!$goods['price'])
                $this->error('请填写商品价格');
        
            if (empty($goodsId) && empty($goods['pic_url'])) {
                $this->error('请上传商品图片');
            }
        
           
        
            $GoodsModel = D('goods');
            $GoodsModel->create();
            if (empty($goodsId)) {
                $GoodsModel->add();
            } else {
                $GoodsModel->where("id='{$goodsId}'")->save();
            }
          
            $this->success('操作成功',U('edit'));
        }
       
       
        $category = D('CategoryGoods')->getGoodsCategory();
     
        $this->assign('category', $category);
        $this->assign('topic', $this->listAll(D('Topic')));
        $this->assign('tags',$this->listAll(D('Tag')));
        $this->assign('goods', D('Goods')->info($goodsId));
   
        $this->display();
    }
    public function status($status = 0){
        $id = array_unique((array)I('id',0));
        $id = is_array($id) ? implode(',',$id) : $id;
        if ( empty($id) ) {
            $this->error('请选择要操作的数据!');
        }
        $where['id'] =   array('in',$id);
      
        $goods = D('Goods')->getGoodsById($goodsId);
        $GoodsModel = new GoodsModel();
         $GoodsModel->where($where)->save(array('status'=>$status ? 0 : 1));
     
        $this->success('操作成功',U('Goods/index'));
    }
    public function del(){
        $id = array_unique((array)I('id',0));       
        $id = is_array($id) ? implode(',',$id) : $id;
        if ( empty($id) ) {
            $this->error('请选择要操作的数据!');
        }
        $where['id'] =   array('in',$id);
        if(M('Goods')->where($where)->delete()){
             $this->success('操作成功');
        }else {
            $this->error('删除失败');
        }
    }
    public function  getItemInfo($url){
        preg_match('/taobao.com/', $url,$t);
        preg_match('/tmall.com/', $url,$tm);
        if(isset($t['0']) || isset($tm['0'])){
            preg_match('/id=\d*/', $url,$data);
            $numIid = trim($data[0],'id=');
            $taobao = new TopApi(C('APP_KEY'), C('APP_SECRET'));
            $item = $taobao->getItemInfo($numIid);
           
            $result = array(
                'errno' =>0,
                'obj'   =>array()
            );
            if(is_array($item)){
                $result['obj']=$item[0];
            }else{
                $result['errno']=1;
            }
            $this->ajaxReturn($result);
        }
        
    }
    
}

?>